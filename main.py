#! /usr/bin/env python

""" This script predicts which countries have an open or controlled border."""
import json
import os
import tempfile
from datetime import datetime
from itertools import combinations
import pandas as pd
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import StandardScaler
# https://www.tensorflow.org/tutorials/structured_data/imbalanced_data
import tensorflow as tf
from tensorflow import keras
import matplotlib as mpl
import matplotlib.pyplot as plt
import platform

mpl.rcParams['figure.figsize'] = (12, 10)
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
pd.options.display.max_columns = 999

if platform.system() == "Darwin":
    command = "sysctl -n machdep.cpu.brand_string"
    if 'Apple M' in command:
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class ImbalancedTensorflow:
    """This class uses open source code from tensorflow.org to run a tensorflow model on our data"""
    def __init__(self):
        self.baseline_history = None
        self.metrics = [
            keras.metrics.TruePositives(name='tp'),
            keras.metrics.FalsePositives(name='fp'),
            keras.metrics.TrueNegatives(name='tn'),
            keras.metrics.FalseNegatives(name='fn'),
            keras.metrics.BinaryAccuracy(name='accuracy'),
            keras.metrics.Precision(name='precision'),
            keras.metrics.Recall(name='recall'),
            keras.metrics.AUC(name='auc'),
        ]
        self.train_features = None

        self.epochs = 100
        self.batch_size = 2048
        self.results = None
        self.run1 = None
        self.run2 = None
        self.run3 = None
        self.run4 = None
        self.run5 = None

    @staticmethod
    def plot_metrics(history):
        """Print a chart of the accuracy of each iteration of the neural network"""
        metrics = ['loss', 'auc', 'precision', 'recall']
        for index, metric in enumerate(metrics):
            name = metric.replace("_", " ").capitalize()
            plt.subplot(2, 2, index + 1)
            plt.plot(
                history.epoch,
                history.history[metric],
                color=colors[0],
                label='Train')
            plt.plot(history.epoch, history.history['val_' + metric],
                     color=colors[0], linestyle="--", label='Val')
            plt.xlabel('Epoch')
            plt.ylabel(name)
            if metric == 'loss':
                plt.ylim([0, plt.ylim()[1]])
            elif metric == 'auc':
                plt.ylim([0.8, 1])
            else:
                plt.ylim([0, 1])

            plt.legend()

    def make_model(self, output_bias=None):
        if output_bias is not None:
            output_bias = tf.keras.initializers.Constant(output_bias)
        model = keras.Sequential([
            keras.layers.Dense(
                16, activation='relu',
                input_shape=(self.train_features.shape[-1],)),
            keras.layers.Dropout(0.5),
            keras.layers.Dense(1, activation='sigmoid',
                               bias_initializer=output_bias),
        ])

        model.compile(
            optimizer=keras.optimizers.Adam(learning_rate=1e-3),
            loss=keras.losses.BinaryCrossentropy(),
            metrics=self.metrics)

        return model

    def tensor(self, train_df, test_df, neg, pos):
        train_df, val_df = train_test_split(train_df, test_size=.2)
        # Form np arrays of labels and features.
        train_labels = np.array(train_df['Status'])
        train_df = train_df.drop(columns='Status')
        val_labels = np.array(val_df['Status'])
        val_df = val_df.drop(columns='Status')
        test_labels = np.array(test_df['Status'])
        test_df = test_df.drop(columns='Status')

        self.train_features = np.array(train_df)
        val_features = np.array(val_df)
        test_features = np.array(test_df)
        scaler = StandardScaler()
        self.train_features = scaler.fit_transform(self.train_features)

        val_features = scaler.transform(val_features)
        test_features = scaler.transform(test_features)

        self.train_features = np.clip(self.train_features, -5, 5)
        val_features = np.clip(val_features, -5, 5)
        test_features = np.clip(test_features, -5, 5)

        early_stopping = tf.keras.callbacks.EarlyStopping(
            monitor='val_auc',
            verbose=1,
            patience=10,
            mode='max',
            restore_best_weights=True)
        model = self.make_model()
        self.results = model.evaluate(
            self.train_features,
            train_labels,
            batch_size=self.batch_size,
            verbose=0)
        initial_bias = np.log([pos / neg])
        model = self.make_model(output_bias=initial_bias)
        self.results = model.evaluate(
            self.train_features,
            train_labels,
            batch_size=self.batch_size,
            verbose=0)
        initial_weights = os.path.join(tempfile.mkdtemp(), 'initial_weights')
        model.save_weights(initial_weights)
        model = self.make_model()
        model.load_weights(initial_weights)
        model.layers[-1].bias.assign([0.0])
        zero_bias_history = model.fit(
            self.train_features,
            train_labels,
            batch_size=self.batch_size,
            epochs=20,
            validation_data=(val_features, val_labels),
            verbose=0)
        model = self.make_model()
        model.load_weights(initial_weights)
        careful_bias_history = model.fit(
            self.train_features,
            train_labels,
            batch_size=self.batch_size,
            epochs=20,
            validation_data=(val_features, val_labels),
            verbose=0)
        model = self.make_model()
        model.load_weights(initial_weights)
        baseline_history = model.fit(
            self.train_features,
            train_labels,
            batch_size=self.batch_size,
            epochs=self.epochs,
            callbacks=[early_stopping],
            validation_data=(val_features, val_labels))
        train_predictions_baseline = model.predict(
            self.train_features, batch_size=self.batch_size)
        test_predictions_baseline = model.predict(
            test_features, batch_size=self.batch_size)
        d = dict()
        d['testfeatures'] = test_features
        d['test_labels'] = test_labels
        d['train_labels'] = train_labels
        d['test_predictions'] = test_predictions_baseline
        d['model'] = model
        return d

    def neuralnetwork(self, df):
        eps = 0.001
        for x in ['GDP_x', 'GDP_y', 'Population_x', 'Population_y', 'GDP', 'Population', 'Average Population',
                  'GDP per capita', 'worse GDP per capita', 'better GDP per capita', 'GDP per capita_x',
                  'GDP per capita_y']:
            df[x] = np.log(df.pop(x) + eps)
        raw_df = df._get_numeric_data()
        neg, pos = np.bincount(raw_df['Status'])
        # Use a utility from sklearn to split and shuffle our dataset.
        shuffled = raw_df.sample(frac=1)
        result = np.array_split(shuffled, 5)
        run1 = self.tensor(
            pd.concat([result[x] for x in [1, 2, 3, 4]]), result[0], neg, pos)
        run2 = self.tensor(
            pd.concat([result[x] for x in [0, 2, 3, 4]]), result[1], neg, pos)
        run3 = self.tensor(
            pd.concat([result[x] for x in [0, 1, 3, 4]]), result[2], neg, pos)
        run4 = self.tensor(
            pd.concat([result[x] for x in [0, 1, 2, 4]]), result[3], neg, pos)
        run5 = self.tensor(
            pd.concat([result[x] for x in [0, 1, 2, 3]]), result[4], neg, pos)
        return [run1, run2, run3, run4, run5, result, df]

    @staticmethod
    def plot_cm(labels, predictions, probability=0.5):
        """Confusion matrix inner logic"""
        matrix = confusion_matrix(labels, predictions > probability)
        plt.figure(figsize=(5, 5))
        sns.heatmap(matrix, annot=True, fmt="d")
        plt.title('Confusion matrix @{:.2f}'.format(probability))
        plt.ylabel('Actual label')
        plt.xlabel('Predicted label')

        print('True Negatives: ', matrix[0][0])
        print('False Positives: ', matrix[0][1])
        print('False Negatives: ', matrix[1][0])
        print('True Positives: ', matrix[1][1])
        print('Total Positive cases: ', np.sum(matrix[1]))

    def plotter(self, model, test_features, test_labels,
                test_predictions_baseline):
        """Plot a confusion matrix"""
        baseline_results = model.evaluate(test_features, test_labels,
                                          batch_size=self.batch_size, verbose=0)
        for name, value in zip(model.metrics_names, baseline_results):
            print(name, ': ', value)
        print()

        return self.plot_cm(test_labels, test_predictions_baseline)


class BorderPrediction:
    """This class generates the dataframe which we can then plug into our neural network."""
    def __init__(self):
        self.test = None

    def initialize_data(self):
        df = pd.read_csv('Untitled spreadsheet - Sheet1.csv')
        df = df.drop(columns='Independence Date')
        df = df.dropna()
        borders = []
        for i in df['Borders']:
            borders.append(i.split('/'))
        within = []
        for i in df['Within']:
            within.append(i.split('/'))
        df['Borders'] = borders
        df['Within'] = within
        # quality of life indicators
        cpi = pd.read_csv('cpi.csv')
        cpi = cpi[['Country', '2015 cpi']]
        cpi = cpi.replace('-', np.nan)
        cpi['2015 cpi'] = [float(x) for x in cpi['2015 cpi']]
        edbi = pd.read_csv('edbiscores.csv')
        edbi = edbi.replace('Russian Federation', 'Russia')
        homicide = pd.read_csv('homicide.csv')
        pfi = pd.read_csv('pfi.csv')[['Country', 'Score']]
        pfi.columns = ['Country', '2018 pfi']
        pfi['2018 pfi'] = [float(x) for x in pfi['2018 pfi']]
        homicide.columns = [
            'Country',
            'Region',
            'Subregion',
            'Homicide Rate',
            'Homicide County',
            'Year',
            'Source']
        qol = pd.merge(cpi, pfi, on='Country', how='outer')
        qol = pd.merge(qol, edbi, on='Country', how='outer')
        qol = pd.merge(qol, homicide, on='Country', how='outer')
        qol['score'] = qol['2015 cpi'] / qol['2018 pfi'] * qol['DB 2019']
        # GDP and Population
        data = pd.read_csv('world_indicators_all_years.csv')
        data = data[data['Time'] == '2016']
        country_names = {'Iran, Islamic Rep.': "Iran",
                         "Korea, Dem. People's Rep.": "North Korea",
                         "Lao PDR": "Laos",
                         "Korea, Rep.": "South Korea",
                         "Macedonia, FYR": "Macedonia",
                         "Myanmar": "Myanmar (Burma)",
                         "Russian Federation": "Russia",
                         "Slovak Republic": "Slovakia",
                         "Syrian Arab Republic": "Syria",
                         "Venezuela, RB": "Venezuela",
                         "Yemen, Rep.": "Yemen"}
        for key, value in country_names.items():
            data = data.replace(key, value)
        data1 = data[['Country Name', 'Population, total [SP.POP.TOTL]', 'Population growth (annual %) [SP.POP.GROW]',
                      'GDP (current US$) [NY.GDP.MKTP.CD]', 'GDP per capita (current US$) [NY.GDP.PCAP.CD]']]
        data1 = data1.replace('..', np.nan)
        for column in [*data1.columns][1:]:
            data1[column] = [float(i) for i in data1[column]]
        data1['Population, total [SP.POP.TOTL]'] = [float(n) for n in data1['Population, total [SP.POP.TOTL]']]
        data1 = pd.merge(
            qol,
            data1,
            left_on='Country',
            right_on='Country Name',
            how='outer')
        organizations = json.loads(open('organizations.json', 'r').read())
        schengen = organizations['Schengen Area']
        common = organizations['Common Travel Area']
        union_state = organizations['Union State']
        peace_and_friendship = organizations['Peace And Friendship']
        ca4 = ['']
        table = [schengen + common + union_state + peace_and_friendship + ca4 + tasman + andean + caricom + gcc + eac,
                 ['Schengen'] * len(schengen) + ['Common'] * len(common) + ['Union State'] * len(union_state) + [
                     'Peace and Friendship'] *
                 len(peace_and_friendship) + ['Central America 4'] * len(ca4) + [
                     'Trans-Tasman Agreement'] * len(tasman) + ['Andean Community'] *
                 len(andean) + len(caricom) * [
                     'Caricom'] + len(gcc) * ['Gulf Cooperation Council'] + ['East African Community'] *
                 len(eac)]
        treaties = pd.DataFrame(table).transpose()
        treaties.columns = ['Country', 'Treaty']
        notreaty = pd.DataFrame(
            [*set(treaties['Country']) ^ set(data1['Country Name'])])
        notreaty['Treaty'] = None
        notreaty.columns = ['Country', 'Treaty']
        treaties = pd.concat([notreaty, treaties])
        data1 = pd.merge(data1, treaties, how='outer')
        # colonies
        british = ['Afghanistan', 'Antigua and Barbuda', 'Australia', 'Bahamas', 'Bahrain', 'Barbados', 'Belize',
                   'Botswana', 'Brunei', 'Cameroon', 'Canada', 'Cyprus', 'Dominica', 'Egypt', 'Swaziland', 'Fiji',
                   'Gambia', 'Ghana', 'Grenada', 'Guyana', 'India', 'Iraq', 'Israel', 'Jamaica', 'Jordan', 'Kenya',
                   'Kiribati', 'Kuwait', 'Lesotho', 'Libya', 'Malawia', 'Malaysia', 'Maldives', 'Malta', 'Mauritius',
                   'Myanmar', 'Nauru', 'New Zealand', 'Nigeria', 'Oman', 'Pakistan', 'Palestine', 'Qatar',
                   'Saint Lucia', 'Saint Kitts and Nevis', 'Saint Vincent and the Grenadines', 'Seychelles',
                   'Sierra Leone', 'Singapore', 'Solomon Islands', 'South Africa', 'Sri Lanka', 'Sudan', 'Tanzania',
                   'Tonga', 'Trinidad and Tobago', 'Tuvalu', 'Uganda', 'United Arab Emirates', 'United States',
                   'Vanuatu', 'Yemen', 'Zambia', 'Zimbabwe', 'Ireland']
        french = ['Haiti', 'Suriname', 'Dominica', 'Saint Kitts and Nevis', 'Grenada',
                  'Saint Vincent and the Grenadines', 'Saint Lucia', 'Morocco', 'Algeria', 'Tunisia', 'Ivory Coast',
                  'Benin', 'Mali', 'Guinea', 'Mauritania',
                  'Niger', 'Senegal', 'Burkina Faso', 'Togo', 'Nigeria', 'Chad', 'Central African Republic', 'Congo',
                  'Gabon', 'Cameroon', 'Sao Tome and Principe', 'Madagascar', 'Mauritius', 'Djibouti', 'Seychelles',
                  'Comoros', 'Laos', 'Cambodia', 'Vietnam', 'Syria', 'Lebanon', 'Yemen', 'Vanuatu']
        spanish = ['Mexico', 'Guatemala', 'El Salvador', 'Nicaragua', 'Honduras', 'Costa Rica', 'Panama', 'Cuba',
                   'Dominican Republic', 'Colombia', 'Venezuela', 'Ecuador', 'Peru', 'Bolivia', 'Paraguay', 'Uruguay',
                   'Argentina', 'Chile', 'Equatorial Guinea', 'Philippines']
        portuguese = [
            'Brazil',
            'Guinea Bissau',
            'Angola',
            'Mozambique',
            'Timor Leste']
        dutch = ['Indonesia']
        table = [british + french + spanish + portuguese + dutch,
                 ['British'] * len(british) + ['French'] * len(french) + ['Spanish'] * len(spanish) + ['Portuguese'] *
                 len(portuguese) + ['Dutch'] * len(dutch)]
        colonies = pd.DataFrame(table).transpose()
        colonies.columns = ['Country', 'Colony']
        nocolony = pd.DataFrame(
            [*set(colonies['Country']) ^ set(data1['Country Name'])])
        nocolony['Colony'] = None
        nocolony.columns = ['Country', 'Colony']
        colonies = pd.concat([nocolony, colonies])
        data1 = pd.merge(data1, colonies, how='outer')
        # Democracy Index from the EIU
        democracy = pd.read_csv('democracy_index_2024.csv')
        democracy.columns = ['Rank', 'Country', 'Democracy Score', 'Electoral process and pluralism',
                             'Functioning of government', 'Political participation',
                             'Political culture', 'Civil liberties', 'Regime type', 'Region[n 1]',
                             'Changes from last year']
        data1 = pd.merge(data1, democracy, on='Country', how='outer')
        for x in ['British', 'Spanish', 'French', 'Portuguese']:
            data1[x] = pd.to_numeric(
                data1['Colony'].replace(
                    x, 1), errors='coerce').fillna(0)
        data1['Europe'] = pd.to_numeric(
            data1['Region'].replace(
                'Europe', 1), errors='coerce').fillna(0)
        # bring it all together
        gini = pd.read_csv('gini.csv').iloc[:217].replace('..', np.nan)
        data1['gini'] = gini[['1990 [YR1990]', '2000 [YR2000]', '2010 [YR2010]', '2011 [YR2011]',
                              '2012 [YR2012]', '2013 [YR2013]', '2014 [YR2014]', '2015 [YR2015]',
                              '2016 [YR2016]', '2017 [YR2017]', '2018 [YR2018]', '2019 [YR2019]'
                              ]].astype(float).mean(axis=1)
        combos = list(combinations(data1['Country'], 2))
        borders = pd.DataFrame(combos).drop_duplicates()
        test2 = pd.merge(pd.merge(borders, data1.drop(columns='Rank'), left_on=0, right_on='Country Name', how='inner'),
                         data1.drop(columns='Rank'), left_on=1, right_on='Country Name', how='inner')
        # More statistics
        test2.columns = ['Country_x', 'Country_y', 'C', '2015 cpi_x', '2018 pfi_x', 'DB_y018_x',
                         'DB_y019_x', 'Region_x', 'Subregion_x', 'Homicide Rate_x',
                         'Homicide County_x', 'Year_x', 'Source_x', 'score_x', 'Country Name_x',
                         'Population_x',
                         'Population growth_x',
                         'GDP_x',
                         'GDP per capita_x', 'Treaty_x', 'Colony_x',
                         'Democracy Score_x', 'Electoral process and pluralism_x',
                         'Functioning of government_x', 'Political participation_x',
                         'Political culture_x', 'Civil liberties_x', 'Regime type_x',
                         'Region[n_x]_x', 'Changes from last year_x', 'British_x', 'Spanish_x', 'French_x',
                         'Portuguese_x', 'Europe_x', 'gini_x', 'Country',
                         '2015 cpi_y', '2018 pfi_y', 'DB_y018_y', 'DB_y019_y', 'Region_y',
                         'Subregion_y', 'Homicide Rate_y', 'Homicide County_y', 'Year_y',
                         'Source_y', 'score_y', 'Country Name_y', 'Population_y', 'Population growth_y', 'GDP_y',
                         'GDP per capita_y', 'Treaty_y', 'Colony_y',
                         'Democracy Score_y', 'Electoral process and pluralism_y',
                         'Functioning of government_y', 'Political participation_y',
                         'Political culture_y', 'Civil liberties_y', 'Regime type_y',
                         'Region[n_x]_y', 'Changes from last year_y', 'British_y', 'Spanish_y', 'French_y',
                         'Portuguese_y', 'Europe_y', 'gini_y']
        test2['Population difference'] = test2['Population_x'] / \
            test2['Population_y']
        test2['Population growth difference'] = test2['Population growth_x'] / \
            test2['Population growth_y']
        test2['GDP difference'] = test2['GDP_x'] / test2['GDP_y']
        test2['GDP per capita difference'] = test2['GDP per capita_x'] / \
            test2['GDP per capita_y']
        test2['Democracy Score difference'] = test2['Democracy Score_x'] / \
            test2['Democracy Score_y']
        test2['Electoral process and pluralism difference'] = test2['Electoral process and pluralism_x'] / test2[
            'Electoral process and pluralism_y']
        test2['Functioning of government difference'] = test2['Functioning of government_x'] / test2[
            'Functioning of government_y']
        test2['Political participation difference'] = test2['Political participation_x'] / \
            test2['Political participation_y']
        test2['Population difference'] = [
            1 / x if x > 1 else x for x in test2['Population difference']]
        test2['Population growth difference'] = [
            1 / x if x > 1 else x for x in test2['Population growth difference']]
        test2['GDP difference'] = [
            1 / x if x > 1 else x for x in test2['GDP difference']]
        test2['GDP per capita difference'] = [
            1 / x if x > 1 else x for x in test2['GDP per capita difference']]
        test2['GDP product'] = test2['GDP_x'] * test2['GDP_y']
        test2['GDP per capita in both'] = (
            test2['GDP_x'] + test2['GDP_y']) / (test2['Population_x'] + test2['Population_y'])
        test2 = test2.drop_duplicates('GDP product').reset_index()
        df1 = test2
        df1['cpi difference'] = df1['2015 cpi_x'] / df1['2015 cpi_y']
        # df1['cpi difference'] = [1/x if x> 1 for x in df1['cpi difference']
        # else x]
        df1['GDP sum'] = df1['GDP_x'] + df1['GDP_y']
        df1['cpi difference'] = [
            1 / x if x > 1 else x for x in df1['cpi difference']]
        df1['db difference'] = df1['DB_y019_x'] / df1['DB_y019_y']
        df1['db difference'] = [
            1 / x if x > 1 else x for x in df1['db difference']]
        df1['pfi difference'] = df1['2018 pfi_x'] / df1['2018 pfi_y']
        df1['pfi difference'] = [
            1 / x if x > 1 else x for x in df1['pfi difference']]
        df1['homicide difference'] = df1['Homicide Rate_x'] / \
            df1['Homicide Rate_y']
        df1['homicide difference'] = [
            1 / x if x > 1 else x for x in df1['homicide difference']]
        # df1['trust difference'] = df1['trust1'] / df1['trust2']
        # df1['trust difference'] = [1/x if x> 1 else x for x in df1['pfi difference']]
        df1['Country Similarity'] = (df1['cpi difference'] + df1['db difference'] + df1['pfi difference'] + df1[
            'homicide difference']) / 4
        df1['average cpi'] = df1[['2015 cpi_x', '2015 cpi_y']].mean(axis=1)
        df1['average db'] = df1[['DB_y019_x', 'DB_y019_y']].mean(axis=1)
        df1['average pfi'] = df1[['2018 pfi_x', '2018 pfi_y']].mean(axis=1)
        df1['average homicide'] = df1[[
            'Homicide Rate_x', 'Homicide Rate_y']].mean(axis=1)
        df1['average Democracy Score'] = df1[[
            'Democracy Score_x', 'Democracy Score_y']].mean(axis=1)
        df1['average Electoral process and pluralism'] = df1[
            ['Electoral process and pluralism_x', 'Electoral process and pluralism_y']].mean(axis=1)
        df1['average Functioning of Government'] = df1[['Functioning of government_x', 'Functioning of government_y'
                                                        ]].mean(axis=1)
        df1['average Political participation'] = df1[[
            'Political participation_x', 'Political participation_y']].mean(axis=1)
        df1['average Civil Liberties'] = df1['Civil liberties_x'] / \
            df1['Civil liberties_y']
        # df1['average trust'] = df1[['trust1','trust2']].mean(axis=1)
        df1['Border Status'] = np.where(
            df1['Treaty_x'] == df1['Treaty_y'], 'Open', 'Closed')
        df1['Combined Population'] = df1['Population_x'] + df1['Population_y']
        df1['GDP per capita'] = df1['GDP sum'] / df1['Combined Population']
        df1['viability'] = df1['average cpi'] * df1['average db'] / \
            (df1['average pfi'] * df1['average homicide'])
        df1['viability2'] = df1['average cpi'] * df1['average db'] * df1['GDP per capita'] / (
            df1['average pfi'] * df1['average homicide'])
        df1 = df1.drop_duplicates('Combined Population').sort_values('viability', ascending=False
                                                                     ).reset_index().drop(columns='index')
        df1['Status'] = df1['Border Status'].replace(
            'Open', 1).replace('Closed', 0)
        df1 = df1.replace(np.inf, np.nan)
        df1['Same Region'] = df1['Region_x'] == df1['Region_y']
        df1['Same Subregion'] = df1['Subregion_x'] == df1['Subregion_y']
        df1['Same Colonial Power'] = df1['Colony_x'] == df1['Colony_y']
        df1['Same Region'] = df1['Same Region'].replace(
            False, 0).replace(
            True, 1)
        df1['Same Subregion'] = df1['Same Subregion'].replace(
            False, 0).replace(
            True, 1)
        df1['Same Colonial Power'] = df1['Same Colonial Power'].replace(
            False,
            0).replace(
            True,
            1)
        test = df1.dropna(subset=['Country_x', 'Country_y', '2018 pfi_x', '2018 pfi_y', 'GDP_x', 'GDP_y',
                                  'GDP per capita_x', 'GDP per capita_y', '2015 cpi_x', '2015 cpi_y', 'DB_y018_x',
                                  'DB_y018_y', 'Homicide Rate_x', 'Homicide Rate_y', 'Democracy Score_x',
                                  'Democracy Score_y', 'Status'])[
            ['Country_x', 'Country_y', 'GDP_x', 'GDP_y', 'GDP per capita_x', 'GDP per capita_y', '2015 cpi_x',
             '2015 cpi_y', 'DB_y018_x', 'DB_y018_y', 'Homicide Rate_x', 'Homicide Rate_y', 'Democracy Score_x',
             'Democracy Score_y', 'Population_x', 'Population_y', '2018 pfi_x', '2018 pfi_y', 'Status', 'Same Region',
             'Same Subregion', 'Same Colonial Power', 'British_x', 'French_x', 'Spanish_x', 'Portuguese_x',
             'British_y', 'Spanish_y', 'French_y', 'Portuguese_y', 'Europe_x', 'Europe_y']]
        for x in ['2015 cpi', 'DB_y018', '2018 pfi',
                  'Homicide Rate', 'Democracy Score', 'Population']:
            test['Average ' + x] = test[[x + '_x', x + '_y']].mean(axis=1)
        test['Population'] = test['Population_x'] + test['Population_y']
        test['GDP'] = test['GDP_x'] + test['GDP_y']
        test['GDP per capita'] = test['GDP'] / test['Population']
        test['worse cpi'] = test[['2015 cpi_y', '2015 cpi_x']].min(axis=1)
        test['worse pfi'] = test[['2018 pfi_x', '2018 pfi_y']].max(axis=1)
        test['worse DB'] = test[['DB_y018_y', 'DB_y018_x']].min(axis=1)
        test['worse homicide'] = test[[
            'Homicide Rate_y', 'Homicide Rate_x']].max(axis=1)
        test['worse GDP per capita'] = test[[
            'GDP per capita_y', 'GDP per capita_x']].min(axis=1)
        test['better cpi'] = test[['2015 cpi_y', '2015 cpi_x']].max(axis=1)
        test['better pfi'] = test[['2018 pfi_x', '2018 pfi_y']].min(axis=1)
        test['better DB'] = test[['DB_y018_y', 'DB_y018_x']].max(axis=1)
        test['better homicide'] = test[[
            'Homicide Rate_y', 'Homicide Rate_x']].min(axis=1)
        test['better GDP per capita'] = test[[
            'GDP per capita_y', 'GDP per capita_x']].max(axis=1)
        test['worse Democracy Score'] = test[[
            'Democracy Score_y', 'Democracy Score_x']].min(axis=1)
        test['better Democracy Score'] = test[[
            'Democracy Score_y', 'Democracy Score_x']].max(axis=1)
        self.test = test
        
        data1.columns = ['Country', '2015 cpi', '2018 pfi', 'DB 2018', 'DB 2019', 'Region',
                         'Subregion', 'Homicide Rate', 'Homicide County', 'Year', 'Source',
                         'score', 'Country Name', 'Population',
                         'Population growth',
                         'GDP',
                         'GDP per capita', 'Treaty', 'Colony',
                         'Rank', 'Democracy Score', 'Electoral process and pluralism',
                         'Functioning of government', 'Political participation',
                         'Political culture', 'Civil liberties', 'Regime type', 'Region[n 1]',
                         'Changes from last year', 'British', 'Spanish', 'French', 'Portuguese',
                         'Europe', 'gini']


def main():
    borders = BorderPrediction()
    borders.initialize_data()
    tensor = ImbalancedTensorflow()
    network = tensor.neuralnetwork(borders.test)
    results_list = []
    for x in [0, 1, 2, 3, 4]:
        print('file %s' % x)
        results = network[5][x]
        results['True'] = network[x]['test_labels']
        results['Predicted'] = network[x]['test_predictions']
        results = pd.merge(network[6][['Country_x', 'Country_y', 'GDP per capita_x', 'GDP per capita_y']], results,
                           on=['GDP per capita_x', 'GDP per capita_y'])  # [['Country_x','Country_y']]
        results_list.append(results)
    results = pd.concat(results_list)
    now = datetime.now()
    results.to_csv('results/%s-%s-%s-%s-%s-results.csv' % (now.year, now.month, now.day, now.hour, now.minute))


if __name__ == "__main__":
    main()
