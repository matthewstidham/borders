import re
import pandas as pd
import numpy as np
import json
from itertools import combinations
from sqlalchemy import create_engine
from warnings import simplefilter

simplefilter(action="ignore", category=pd.errors.PerformanceWarning)

pd.options.mode.copy_on_write = True


# pd.set_option('future.no_silent_downcasting', True)


class InitializeData:
    def __init__(self):
        self.organizations = self.load_organizations()
        self.visa_policy_level = 7
        self.replacements = {'Akrotiri and Dhekelia (UK)':'Akrotiri and Dhekelia',
                             'American Samoa (US)':'American Samoa',
                             'Anguilla (UK)':'Anguilla',
                             'Antigua And Barbuda': 'Antigua and Barbuda',
                             'Aruba (Netherlands)':'Aruba',
                             'Åland (Finland)':'Åland Islands',
                             'Bahamas, The': 'Bahamas',
                             'Bermuda (UK)': 'Bermuda',
                             'Bolivia, Plurinational State of': 'Bolivia',
                             'Bolivia (Plurinational State of)': 'Bolivia',
                             'Bosnia and Hercegovina': 'Bosnia and Herzegovina',
                             'Bosnia And Herzegovina': 'Bosnia and Herzegovina',
                             'Bosnia-Herzegovina': 'Bosnia and Herzegovina',
                             'British Indian Ocean Territory': 'British Indian Ocean Territory (UK)',
                             'Brunei Darussalam': 'Brunei',
                             'Burma/Myanmar': 'Myanmar (Burma)',
                             'Cabo Verde': 'Cape Verde',
                             'Cayman Islands (UK)':'Cayman Islands',
                             'China, Hong Kong Special Administrative Region': 'Hong Kong',
                             'China, Macao Special Administrative Region': 'Macau',
                             'Christmas Island (Australia)': 'Christmas Island',
                             'Cocos (Keeling) Islands (Australia)':'Cocos (Keeling) Islands',
                             'Congo': 'Republic of the Congo',
                             'Congo (Brazzaville)': 'Republic of the Congo',
                             'Congo, DR': 'Democratic Republic of the Congo',
                             'Congo, Dem. Rep.': 'Democratic Republic of the Congo',
                             'Congo, Democratic Republic of the': 'Democratic Republic of the Congo',
                             'Congo, Rep.': 'Republic of the Congo',
                             'Congo, Republic': 'Republic of the Congo',
                             'Congo-Brazzaville':"Republic of the Congo",
                             'Curaçao': 'Curacao',
                             'Curaçao (Netherlands)': 'Curacao',
                             'Czech Republic': 'Czechia',
                             "Cote d'Ivoire":"Côte d'Ivoire",
                             'D. R. Congo': 'Democratic Republic of the Congo',
                             'DR Congo': 'Democratic Republic of the Congo',
                             'Democratic Republic of Congo': 'Democratic Republic of the Congo',
                             'Democratic Republic Of The Congo': 'Democratic Republic of the Congo',
                             "East Timor": "Timor-Leste",
                             "Eswatini (Swaziland)": "Eswatini",
                             "Timor Leste":"Timor-Leste",
                             'Easter Island (Chile)': 'Easter Island',
                             'Egypt, Arab Rep.': 'Egypt',
                             'Falkland Islands (Malvinas)': 'Falkland Islands',
                             'Falkland Islands (UK)': 'Falkland Islands',
                             'Faroe Islands (Denmark)': 'Faroe Islands',
                             'Federated States of Micronesia': 'Micronesia',
                             'F.S. Micronesia': 'Micronesia',
                             'French Polynesia': 'French Polynesia (France)',
                             'Gambia': 'The Gambia',
                             'Gambia, The': 'The Gambia',
                             'Gibraltar': 'Gibraltar (UK)',
                             'Greenland (Denmark)': 'Greenland',
                             'Guam': 'Guam (US)',
                             "Guernsey (UK)": "Guernsey",
                             'Guinea Bissau': 'Guinea-Bissau',
                             'Holy See': 'Vatican City',
                             'Hong Kong SAR, China': 'Hong Kong',
                             'Iran, Islamic Rep.': 'Iran',
                             'Iran, Islamic Republic of': 'Iran',
                             'Iran (Islamic Republic of)': 'Iran',
                             "Isle of Man (UK)": "Isle of Man",
                             'Ivory Coast': "Côte d'Ivoire",
                             'Jersey (UK)': "Jersey",
                             'Korea, Dem. People?s Rep.': 'North Korea',
                             'Korea, Dem. People’s Rep.': 'North Korea',
                             "Korea, Democratic People's Republic of": 'North Korea',
                             'Korea, North': 'North Korea',
                             'Korea, Rep.': 'South Korea',
                             'Korea, Republic of': 'South Korea',
                             'Korea, South': 'South Korea',
                             'Kyrgyz Republic': 'Kyrgyzstan',
                             'Lao PDR': 'Laos',
                             "Lao People's Democratic Republic": 'Laos',
                             'Macao': 'Macau',
                             'Macao SAR, China': 'Macau',
                             "Macau (China)": "Macau",
                             'Macedonia': 'North Macedonia',
                             'Macedonia, FYR': 'North Macedonia',
                             'Micronesia, Fed. Sts.': 'Micronesia',
                             'Micronesia, Federated States of': 'Micronesia',
                             'Moldova, Republic of': 'Moldova',
                             'Republic of Moldova': 'Moldova',
                             'Montserrat': 'Montserrat (UK)',
                             'Myanmar (Burma)': 'Myanmar',
                             'Nagorno-Karabakh Republic': 'Artsakh',
                             'Netherlands, Kingdom of the': 'Netherlands',
                             'New Caledonia': 'New Caledonia (France)',
                             'Norfolk Island (Australia)': "Norfolk Island",
                             'Northern Mariana Islands (US)':'Northern Mariana Islands',
                             'Palestine, State of': 'Palestine',
                             'Pitcairn': 'Pitcairn Islands',
                             'Pitcairn Islands (UK)': 'Pitcairn Islands',
                             'Puerto Rico (US)': 'Puerto Rico',
                             'Puerto Rico (U.S.)': 'Puerto Rico',
                             'Republic Of The Congo': 'Republic of the Congo',
                             "Republic of Korea": "South Korea",
                             'Russian Federation': 'Russia',
                             'Saint Barthélemy': 'Saint Barthélemy (France)',
                             'Saint Helena, Ascension and Tristan da Cunha':
                                 'Saint Helena, Ascension and Tristan da Cunha (UK)',
                             'Saint Kitts And Nevis': 'Saint Kitts and Nevis',
                             'Saint Martin (French part)': 'Saint Martin',
                             'Saint Martin (France)': 'Saint Martin',
                             'St. Martin (French part)': "Saint Martin",
                             'Saint Pierre and Miquelon (France)':'Saint Pierre and Miquelon',
                             'Saint Vincent And The Grenadines': 'Saint Vincent and the Grenadines',
                             'Sao Tome And Principe': 'São Tomé and Príncipe',
                             'Sao Tome and Principe': 'São Tomé and Príncipe',
                             'Sint Maarten (Dutch part)': 'Sint Maarten (Netherlands)',
                             'Sint Maarten':'Sint Maarten (Netherlands)',
                             'Slovak Republic': 'Slovakia',
                             'St. Kitts and Nevis': 'Saint Kitts and Nevis',
                             'St. Lucia': 'Saint Lucia',
                             'St. Vincent and the Grenadines': 'Saint Vincent and the Grenadines',
                             'South Georgia and the South Sandwich Islands (UK)':'South Georgia and the South Sandwich Islands',
                             'Svalbard (Norway)':'Svalbard',
                             'Swaziland': 'Eswatini',
                             'Syrian Arab Republic': 'Syria',
                             'Taiwan, China': 'Taiwan',
                             'Taiwan, Province of China': 'Taiwan',
                             'Tanzania, United Republic of': 'Tanzania',
                             'Tokelau': 'Tokelau (New Zealand)',
                             'Trinidad And Tobago': 'Trinidad and Tobago',
                             'Türkiye': 'Turkey',
                             'Turks and Caicos Islands': 'Turks and Caicos Islands (UK)',
                             'United Kingdom of Great Britain and Northern Ireland': 'United Kingdom',
                             'United States of America': 'United States',
                             'United Republic of Tanzania': 'Tanzania',
                             'Venezuela, Bolivarian Republic of': 'Venezuela',
                             'Venezuela (Bolivarian Republic of)': 'Venezuela',
                             'Venezuela, RB': 'Venezuela',
                             'Viet Nam': 'Vietnam',
                             'British Virgin Islands (UK)': 'British Virgin Islands',
                             'Virgin Islands (British)': 'British Virgin Islands',
                             'Virgin Islands (U.S.)': 'U.S. Virgin Islands',
                             'US Virgin Islands (US)': 'U.S. Virgin Islands',
                             'U.S. Virgin Islands (US)': 'U.S. Virgin Islands',
                             'United States Virgin Islands': 'U.S. Virgin Islands',
                             '\xa0Vatican City':"Vatican City",
                             'Wallis and Futuna': 'Wallis and Futuna (France)',
                             'Yemen, Rep.': 'Yemen',
                             'eSwatini': 'Eswatini'}

    gay_marriage = ['Canada', 'United States', 'Mexico', 'Costa Rica', 'Cuba', 'Colombia', 'Ecuador',
                    'Brazil', 'Uruguay', 'Argentina', 'Chile', 'Denmark', 'Iceland', 'Sweden', 'Finland',
                    'Norway', 'Estonia', 'Germany', 'Netherlands', 'Belgium', 'Luxembourg', 'France', 'Andorra',
                    'Spain', 'Ireland', 'United Kingdom', 'Portugal', 'Austria', 'Switzerland', 'Greece', 'Malta',
                    'South Africa', 'Taiwan', 'Australia', 'New Zealand', 'Antigua and Barbuda', 'Slovenia']
    civil_union = ['Italy', 'Monaco', 'Croatia', 'Montenegro', 'Czechia', 'Latvia', 'Cyprus', 'Bolivia', 'San Marino']
    gay_punishment = ['Morocco', 'Mauritania', 'Senegal', 'The Gambia', 'Algeria', 'Tunisia', 'Libya', 'Egypt',
                      'Chad', 'Nigeria', 'Cameroon', 'Guinea', 'Liberia', 'Ghana', 'Togo', 'Nigeria', 'Cameroon',
                      'Eswatini','Grenada','Jamaica','Saint Lucia','Saint Vincent and the Grenadines',
                      'Chad', 'Sudan', 'South Sudan', 'Saudi Arabia', 'Sierra Leone', 'Syria', 'Kuwait',
                      'Lebanon', 'Iran', 'Afghanistan', 'Uzbekistan', 'Turkmenistan', 'Eritrea', 'Ethiopia',
                      'Somalia', 'Kenya', 'Uganda', 'Burundi', 'Tanzania', 'Zambia', 'Zimbabwe', 'Malawi',
                      'Comoros', 'Maldives', 'Sri Lanka', 'Malaysia', 'Brunei', 'Papua New Guinea', 'Guyana',
                      'Yemen', 'Oman', 'Iraq', 'Pakistan', 'Bangladesh', 'Solomon Islands']
    capital_punishment = ['Nigeria', 'Iran', 'Russia', 'Afghanistan', 'Saudi Arabia', 'United Arab Emirates',
                          'Libya', 'Algeria', 'Cameroon', 'Eritrea', 'Tanzania', 'Uganda', 'Qatar']

    @staticmethod
    def floaty(x):
        try:
            return float(x)
        except ValueError:
            return np.nan

    def load_cpi(self):  # Load Corruption Perceptions Index
        cpi = pd.read_csv('cpi2021.csv')
        # cpi.columns = ['Country','2016 cpi']
        cpi['Country'] = cpi['Country'].replace(self.replacements)
        return cpi

    def load_pfi(self):  # Load Press Freedom Index
        pfi = pd.read_csv('pfi2021.csv')
        pfi.columns = ['Country ISO3', 'Country', 'Indicator Id', 'Indicator',
                       'Subindicator Type', 'pfi 2001', 'pfi 2002', 'pfi 2003', 'pfi 2004', 'pfi 2005', 'pfi 2006',
                       'pfi 2007', 'pfi 2008', 'pfi 2009', 'pfi 2012', 'pfi 2013', 'pfi 2014', 'pfi 2015', 'pfi 2016',
                       'pfi 2017',
                       'pfi 2018', 'pfi 2019', 'pfi 2020', 'pfi 2021']
        pfi = pfi[pfi['Indicator'] == 'Press Freedom Index']
        for key, value in self.replacements.items():
            pfi['Country'] = pfi['Country'].replace(key, value)
        pfi_2024 = pd.read_csv("pfi_2024.csv", sep=';', decimal=',')
        pfi_2024.replace({'Country_EN': self.replacements}, inplace=True)
        pfi_2024 = pfi_2024[['Score', 'Country_EN']]
        pfi_2024.columns = ['pfi 2024', 'Country']
        pfi = pd.merge(pfi, pfi_2024, on='Country',how='outer')
        pfi['pfi 2024'] = [100 - x for x in pfi['pfi 2024']]
        return pfi

    def load_abortion(self):
        abortion = pd.read_csv('abortion.csv')
        abortion = abortion.replace('Yes', 1).replace('No', 0)
        abortion['Country'] = [x.lstrip(' ') for x in abortion['Region or country']]
        abortion['Country'].replace(self.replacements,inplace=True)
        return abortion

    def load_homicide(self):
        homicide = pd.read_csv('homicide.csv')
        homicide.columns = ['Country', 'Region', 'Subregion', 'Homicide Rate', 'Homicide County', 'Year', 'Source']
        homicide['Country'] = homicide['Country'].replace(self.replacements)
        return homicide

    @staticmethod
    def load_organizations():
        d = json.loads(open("organizations.json").read())
        return d

    def load_democracy(self):
        democracy = pd.read_csv('democracy_index_2024.csv')
        democracy.replace({'Country': self.replacements}, inplace=True)
        return democracy

    def world_bank_data(self):
        data = pd.read_csv('world_indicators_all_years.csv')
        data.replace({'Country Name': self.replacements}, inplace=True)
        data.replace('..', np.nan, inplace=True)
        for x in [*data.columns][1:]:
            try:
                data[x] = [float(i) for i in data[x]]
            except:
                pass
        data.columns = ['Time', 'Time Code', 'Country', 'Country Code',
                        'Population',
                        'Population growth',
                        'Surface area',
                        'Poverty headcount ratio at national poverty lines',
                        'GNI, Atlas method',
                        'GNI per capita, Atlas method',
                        'GNI, PPP',
                        'GNI per capita, PPP',
                        'Income share held by lowest 20%',
                        'Life expectancy at birth',
                        'Fertility rate, total (births per woman)',
                        'Adolescent fertility rate (births per 1,000 women ages 15-19',
                        'Contraceptive prevalence, any methods (% of women ages 15-49)',
                        'Births attended by skilled health staff (% of total)',
                        'Mortality rate, under-5 (per 1,000 live births)',
                        'Prevalence of underweight, weight for age',
                        'Immunization, measles',
                        'Primary completion rate, total',
                        'School enrollment, secondary (% gross)',
                        'School enrollment, primary and secondary (gross), gender parity index (GPI)',
                        'Prevalence of HIV, total (% of population ages 15-49)',
                        'Forest area (sq. km)',
                        'Water productivity, total (constant 2010 US$ GDP per cubic meter of total '
                        'freshwater withdrawal) [ER.GDP.FWTL.M3.KD]',
                        'Improved water source (% of population with access) [SH.H2O.SAFE.ZS]',
                        'Improved sanitation facilities (% of population with access) [SH.STA.ACSN]',
                        'Energy use (kg of oil equivalent per capita) [EG.USE.PCAP.KG.OE]',
                        'CO2 emissions (metric tons per capita)',
                        'Electric power consumption (kWh per capita) [EG.USE.ELEC.KH.PC]',
                        'GDP',
                        'GDP growth',
                        'Inflation, GDP deflator (annual %) [NY.GDP.DEFL.KD.ZG]',
                        'Agriculture, value added (% of GDP) [NV.AGR.TOTL.ZS]',
                        'Industry, value added (% of GDP) [NV.IND.TOTL.ZS]',
                        'Services, etc., value added (% of GDP) [NV.SRV.TETC.ZS]',
                        'Exports of goods and services (% of GDP) [NE.EXP.GNFS.ZS]',
                        'Imports of goods and services (% of GDP) [NE.IMP.GNFS.ZS]',
                        'Gross capital formation (% of GDP) [NE.GDI.TOTL.ZS]',
                        'Revenue, excluding grants (% of GDP) [GC.REV.XGRT.GD.ZS]',
                        'Start-up procedures to register a business (number) [IC.REG.PROC]',
                        'Market capitalization of listed domestic companies (% of GDP) [CM.MKT.LCAP.GD.ZS]',
                        'Military expenditure (% of GDP) [MS.MIL.XPND.GD.ZS]',
                        'Mobile cellular subscriptions (per 100 people) [IT.CEL.SETS.P2]',
                        'High-technology exports (% of manufactured exports) [TX.VAL.TECH.MF.ZS]',
                        'Merchandise trade (% of GDP) [TG.VAL.TOTL.GD.ZS]',
                        'Net barter terms of trade index (2000 = 100) [TT.PRI.MRCH.XD.WD]',
                        'External debt stocks, total (DOD, current US$) [DT.DOD.DECT.CD]',
                        'Total debt service (% of GNI) [DT.TDS.DECT.GN.ZS]',
                        'Net migration',
                        'Personal remittances, paid (current US$) [BM.TRF.PWKR.CD.DT]',
                        'Foreign direct investment, net inflows (BoP, current US$) [BX.KLT.DINV.CD.WD]',
                        'Net ODA received per capita (current US$) [DT.ODA.ODAT.PC.ZS]',
                        'GDP per capita',
                        'Foreign direct investment, net (BoP, current US$) [BN.KLT.DINV.CD]',
                        'Inflation, consumer prices (annual %) [FP.CPI.TOTL.ZG]']
        data = data.dropna(subset=['Country', 'GDP', 'GDP per capita', 'Population']
                           ).drop_duplicates(subset=['Country'], keep='last')
        return data

    @staticmethod
    def geo_decoder(x):
        value = x.split('°')
        value[0] = int(value[0])
        value[1] = value[1].split('′')
        value[0] = value[0] + float(value[1][0]) / 60
        if 'S' in value[1][1]:
            value[0] *= -1
        return value[0]

    def northernmost(self):
        north = pd.read_csv('northernmost.csv')
        south = pd.read_csv('southernmost.csv')
        north = pd.merge(north, south, on='Country')
        north['Southernmost point'] = [self.geo_decoder(x) for x in north['Southernmost point']]
        north['Northernmost point'] = [self.geo_decoder(x) for x in north['Northernmost point']]
        north.columns = ['Country', 'Northernmost', 'Northernmost point', 'Southernmost', 'Southernmost point']
        return north

    def streetview(self):
        streetview = pd.read_csv('streetview.csv')
        streetview['Streetview'] = True
        streetview['Country'] = [x.strip(' ') for x in streetview['Country']]
        streetview['Country'] = streetview['Country'].replace(self.replacements)
        return streetview

    def load_area(self):
        area = pd.read_csv('area.csv')

        def splitter(x):
            if type(x) is str:
                return x.split(' ')[0].replace(',', '')
            else:
                return x

        for var in ['Total Area', 'Land', 'Water']:
            area[var] = [float(splitter(x)) for x in area[var]]
        area['Country'] = [x.strip(' ').strip('\xa0') for x in area['Country']]
        area['Country'] = area['Country'].replace(self.replacements)
        return area

    def load_gini(self):
        gini = pd.read_csv('gini.csv')
        gini.columns = ['Series Name', 'Series Code', 'Country', 'Country Code',
                        'GINI 1990', 'GINI 2000', 'GINI 2010', 'GINI 2011',
                        'GINI 2012', 'GINI 2013', 'GINI 2014', 'GINI 2015',
                        'GINI 2016', 'GINI 2017', 'GINI 2018', 'GINI 2019']
        gini.replace({'Country': self.replacements}, inplace=True)

        return gini

    def load_sipri(self):
        sipri = pd.read_excel('SIPRI-Milex-data-2021-2023.xlsx', sheet_name='Current US$')
        sipri.columns = sipri.iloc[4]
        sipri.replace({'...': np.nan, 'xxx': np.nan}, inplace=True)
        sipri = sipri.iloc[5:]  # .dropna(subset=[2022])
        sipri.columns = ['Country', 'Notes', 'SIPRI 2021', 'SIPRI 2022']
        for column in ['SIPRI 2021', 'SIPRI 2022']:
            sipri[column] = [x * 10 ** 6 for x in sipri[column]]
        sipri.replace({'Country': self.replacements}, inplace=True)
        return sipri[['Country', 'SIPRI 2021', 'SIPRI 2022']].copy()

    def load_education(self):
        # source: http://data.uis.unesco.org/
        tertiary = pd.read_csv('NATMON_DS_27092024140158666.csv')
        tertiary = tertiary[tertiary[
                                'Indicator'] == 'Mean years of schooling (ISCED 1 or higher), '
                                                'population 25+ years, both sexes'].sort_values(
            'Time', ascending=False).drop_duplicates('Country')
        tertiary.replace({'Country': self.replacements}, inplace=True)
        tertiary.columns = ['NATMON_IND', 'Indicator', 'LOCATION', 'Country', 'TIME', 'Time',
                            'Mean Years of Schooling', 'Flag Codes', 'Flags']
        return tertiary

    def gay_rights(self, df: pd.DataFrame, numeric=False):
        countries = df['Country'].to_list()
        gay_rights = {}
        if numeric:
            for country in countries:
                if country in self.gay_marriage:
                    gay_rights[country] = 2
                if country in self.civil_union:
                    gay_rights[country] = 1
                if country in self.capital_punishment:
                    gay_rights[country] = -2
                elif country in self.gay_punishment:
                    gay_rights[country] = -1
        else:
            for country in countries:
                if country in self.gay_marriage:
                    gay_rights[country] = 'Marriage'
                if country in self.civil_union:
                    gay_rights[country] = 'Civil Union'
                if country in self.capital_punishment:
                    gay_rights[country] = 'Death'
                elif country in self.gay_punishment:
                    gay_rights[country] = 'Prison'
        gay_rights = pd.DataFrame(gay_rights.items())
        gay_rights.columns = ['Country', 'Gay rights']
        return gay_rights

    @staticmethod
    def load_tobacco():
        tobacco = pd.read_csv('tobacco.csv')
        tobacco.columns = ['Country', 'Tobacco 2000', 'Tobacco 2020']
        return tobacco

    def visa_policy(self):
        # https://github.com/ChengCPU/visa-map/blob/main/public/visaPolicy.json
        iso = pd.read_csv('iso_3166_1.csv')
        iso['Country'] = iso['Country'].replace(self.replacements)
        visa_policy = json.loads(open('visaPolicy.json').read())
        visa_policy = {re.sub(r'(?<!^)(?=[A-Z])', ' ', key).title(): value for key, value in visa_policy.items()}
        policy = pd.DataFrame(visa_policy).reset_index()
        policy = pd.merge(iso, policy, right_on='index', left_on='Code', how='outer').dropna(subset=['Country'])
        policy['Country'] = policy['Country'].replace(self.replacements)
        policy.index = policy['Country']
        policy = policy.transpose().drop(index=['Code', 'index']).reset_index()
        policy.columns = [self.replacements[x] if x in self.replacements else x for x in policy.columns]
        policy = policy.drop(0).rename(columns={'index': 'Country'})
        policy['Country'] = policy['Country'].replace(self.replacements)
        policy.index = policy['Country']
        policy.drop(columns=['Country'], inplace=True)
        for column in policy.columns:
            policy[column] = [float(x) for x in policy[column]]
        return policy

    def total_visa_free(self):
        policy = self.visa_policy()
        visa_free = policy.copy()
        for column in policy.columns:
            try:
                visa_free[column] = [False if x > self.visa_policy_level else True for x in visa_free[column]]
            except TypeError:
                pass
            except KeyError:
                print(column)
        total_visa_free = pd.DataFrame(visa_free._get_numeric_data().sum().to_dict().items()).sort_values(1)
        total_visa_free.columns = ['Country', 'Visa free to enter']
        return total_visa_free

    def travel_freedom(self):
        policy = self.visa_policy().transpose()
        visa_free = policy.copy()
        for column in policy.columns:
            try:
                visa_free[column] = [False if x > self.visa_policy_level else True for x in visa_free[column]]
            except TypeError:
                pass
            except KeyError:
                print(column)
        freedom = pd.DataFrame(visa_free._get_numeric_data().sum().to_dict().items()).sort_values(1)
        freedom.columns = ['Country', 'Visa free for passport']
        return freedom

    def gun_laws(self):
        guns = pd.read_excel('gun_laws.xlsx').dropna(subset=['Country'])
        guns['Country'] = [re.sub('(\[(.*?)\])', '', x).strip(' ') for x in guns['Country']]
        guns['Country'] = guns['Country'].replace(self.replacements)

        def handguns(value):
            if value == 'No':
                return 0
            if value == 'Yes' or value == 'Permitless':
                return 5
            if type(value) is str:
                if 'Yes' in value:
                    return 4
            else:
                return 3

        for column in ['Long guns (exc. semi- and full-auto)', 'Handguns',
                       'Semi-automatic rifles', 'Fully automatic firearms', 'Open carry']:
            guns[f"{column} score"] = [handguns(x) for x in guns['Handguns']]
        guns['gun score'] = guns[[x for x in guns.columns if 'score' in x]].sum(axis=1)
        return guns

    def load_wgi(self):
        wgi = pd.read_excel('wgidataset.xlsx')
        wgi.replace('..', np.nan, inplace=True)
        wgi['estimate'] = [float(x) for x in wgi['estimate']]
        wgi = pd.pivot_table(wgi.replace('..', np.nan), values='estimate', index=['countryname', 'year'],
                             columns=['indicator'])
        wgi_latest = wgi.reset_index().drop_duplicates('countryname', keep='last')
        wgi_latest.replace({'countryname': self.replacements}, inplace=True)
        wgi_latest.rename(columns={'countryname':'Country'}, inplace=True)
        return wgi_latest

    def main(self, abortion_data=False, visa_policy=False, english=True, sub_national_data=False, v_dem=False):
        cpi = self.load_cpi()
        pfi = self.load_pfi()
        homicide = self.load_homicide()
        wgi = self.load_wgi()
        df = pd.merge(cpi, pfi, on='Country', how='outer')
        df = pd.merge(df, wgi, on='Country')
        df = pd.merge(df, homicide, on='Country', how='outer')
        tertiary = self.load_education()
        df = pd.merge(df, tertiary[['Country', 'Mean Years of Schooling']], on='Country',
                      how='outer')
        democracy = self.load_democracy()
        df = pd.merge(df, democracy, on='Country', how='outer')  # .dropna(subset=['Country'])
        data = self.world_bank_data()
        df = pd.merge(df, data, on='Country', how='outer')
        # df.dropna(subset=['CPI score 2021'], inplace=True)

        df['Overall Score'] = [self.floaty(x) for x in df['Overall Score']]

        # df['Democracy DTF'] = df['Overall Score'] - min(df[df['Country'].isin(schengen)]['Overall Score'])

        capitals = pd.read_csv('capitals.csv')
        capitals['Country'] = capitals['Country'].replace(self.replacements)
        df = pd.merge(df, capitals.drop_duplicates(subset=['Country']), on='Country', how='outer')

        presidential_trips = pd.read_csv('presidential_trips.csv')
        presidential_trips.columns = ['Region', 'Country', 'Presidential Trips', 'Trips']
        df = pd.merge(df, presidential_trips, on='Country', how='outer')

        north = self.northernmost()
        north.replace(self.replacements, inplace=True)
        df = pd.merge(df, north, on='Country', how='outer')

        if abortion_data:
            abortion = self.load_abortion()
            df = pd.merge(df, abortion, left_on='Country', right_on='Region or country', how='outer')

        gini = self.load_gini()
        df = pd.merge(df, gini, on='Country', how='outer')

        leave = pd.read_csv("paid_leave.csv")
        leave.replace(self.replacements, inplace=True)
        df = pd.merge(df, leave, on='Country', how='outer')

        doing_business = pd.read_csv('edbi.csv')
        doing_business.replace(self.replacements, inplace=True)
        df = pd.merge(df, doing_business, on='Country', how='outer')

        edbi = pd.read_csv('edbiscores.csv')
        edbi.replace(self.replacements, inplace=True)
        df = pd.merge(df, edbi, on='Country', how='outer')

        gsv = self.streetview()
        df = pd.merge(df, gsv, on='Country', how='outer')

        df.replace({'Streetview': {np.nan: False}}, inplace=True)

        area = self.load_area()
        df = pd.merge(df, area, on='Country', how='outer')

        df['Population Density'] = df['Population'] / df['Total Area']

        sipri = self.load_sipri()
        df = pd.merge(df, sipri, on='Country', how='outer')

        df['Military Expenditure per capita'] = df['SIPRI 2022'] / df['Population']
        df['Military Expenditure % of GDP'] = df['SIPRI 2022'] / df['GDP']

        gay_rights = self.gay_rights(df=df)
        df = pd.merge(df, gay_rights, on='Country', how='outer')
        df.replace({'Gay rights': {np.nan: 0}}, inplace=True)

        islam = self.load_islam()
        df = pd.merge(df, islam, on='Country', how='outer').dropna(subset=['Country'])
        df['Muslim percentage of total population'] = df['Muslim Population'] / df['Population']

        tobacco = self.load_tobacco()
        tobacco.replace(self.replacements, inplace=True)
        df = pd.merge(df, tobacco, on='Country', how='outer')
        df.replace('..', np.nan, inplace=True)

        life_expectancy = pd.read_csv('life_expectancy.csv')
        life_expectancy.replace(self.replacements, inplace=True)
        df = pd.merge(df, life_expectancy, on='Country', how='outer')

        for country, name in self.replacements.items():
            df.replace({'Country': {country: name}}, inplace=True)

        gini = [*df[['GINI 1990',
                     'GINI 2000',
                     'GINI 2010',
                     'GINI 2011',
                     'GINI 2012',
                     'GINI 2013',
                     'GINI 2014',
                     'GINI 2015',
                     'GINI 2016',
                     'GINI 2017',
                     'GINI 2018',
                     'GINI 2019']].iterrows()]
        latest_gini = {}
        for row in gini:
            values = [*{key: float(value) for key, value in row[1].to_dict().items() if float(value) > 0}.values()]
            if values:
                latest_gini[row[0]] = values[-1]
            else:
                latest_gini[row[0]] = np.nan
        df['latest gini'] = latest_gini

        orgs = ['Former British colony', 'Former French colony', 'Former Spanish colony', 'Former Soviet Union',
                'Rio Pact', 'Schengen Area', 'NATO', 'European Union', 'Non-NATO allies', 'Other US defense agreements']
        if sub_national_data:
            state_level = SubNational()
            provinces = state_level.return_all()
            df = pd.concat([provinces, df])
            df['State'] = df['State'].fillna(df['Country'])
            for org_value in orgs:
                df[org_value] = {key: (state in self.organizations[org_value]) for key, state in df['State'].items()}
        else:
            for org_value in orgs:
                df[org_value] = {key: (country in self.organizations[org_value]) for key, country in
                                 df['Country'].items()}

        temp = dict()
        for item in df[
            ['Country', 'Former French colony', 'Former British colony',
             'Former Spanish colony', 'Former Soviet Union']].iterrows():
            s = []
            if item[1]['Former French colony']:
                s.append('France')
            if item[1]['Former British colony']:
                s.append('British')
            if item[1]['Former Spanish colony']:
                s.append('Spain')
            if item[1]['Former Soviet Union']:
                s.append('Russia')
            if s:
                temp[item[0]] = (','.join(s))
            else:
                temp[item[0]] = 'Other'
        df['Colonial history'] = temp
        df = df.replace('..', np.nan)
        systems = pd.read_csv('systems_of_government.csv').replace(np.nan, 0)
        systems['Country'] = systems['Country'].replace(self.replacements)
        df = pd.merge(df, systems, on='Country', how='outer')
        if visa_policy:
            visa_free = self.visa_policy()
            if 'State' in df.columns:
                df = pd.merge(df, visa_free, left_on='State', right_on='Country', how='outer')
            else:
                df = pd.merge(df, visa_free, on='Country', how='outer')
            df = pd.merge(df, self.total_visa_free(), on='Country',how='outer')
            df = pd.merge(df, self.travel_freedom(), on='Country',how='outer')

        if english:
            english = pd.read_csv('english.csv')
            english['Country'] = english['Country'].replace(self.replacements)
            df = pd.merge(df, english, on='Country', how='outer')
            df[english.columns] = df[english.columns].fillna(0)

        if 'State' in df.columns:
            target_countries = ['Australia', 'Brazil', 'Canada', 'China', 'Germany', 'Russia', 'United States']
            targets = df[df['Country'].isin(target_countries)][
                ['Country', 'State', 'Region_x', 'Region_y', 'Region']].dropna().transpose()
            targets.columns = targets.loc['Country']
            targets = targets.to_dict()
            for column in ['Region', 'Region_x', 'Region_y']:
                result = {}
                for x in df.iterrows():
                    if x[1]['State'] in targets:
                        country = x[1]['State']
                        result[x[0]] = targets[country][column]
                    else:
                        result[x[0]] = x[1][column]
                df[column] = result
        df = pd.merge(df, self.gun_laws(), on=['Country'], how='outer')
        if v_dem:
            v_dem = pd.read_csv('V-Dem-CY-Full+Others-v14.csv')
            v_dem['country_name'].replace(self.replacements, inplace=True)
            df = pd.merge(df, v_dem.sort_values('year', ascending=False).drop_duplicates('country_name'),
                          left_on='Country', right_on='country_name', how='outer')
        return df

    def country_combos(self):
        df = self.main()

        border = pd.read_csv("border_length.csv")
        border = border.ffill()
        border = border[border['length'] > 0].drop_duplicates()
        neighbor = [x.replace('\xa0', '').split(':') for x in border['country2']]
        length = [x[1].strip(' ').split('km')[0].replace(',', '') for x in neighbor]
        country = [x[0].strip(' ') for x in neighbor]
        length = [float(x) for x in length]
        border['country2'] = country
        border['length2'] = length
        border['Country'] = [x.strip('\xa0').strip(' ') for x in border['Country']]

        replacement = {'Denmark (constituent country)': 'Denmark',
                       'Denmark, Kingdom of': 'Denmark',
                       "People's Republic of China": 'China',
                       'France (including French overseas departments, collectivities, and territories)': 'France',
                       'Netherlands (constituent country)': 'Netherlands',
                       'Netherlands, Kingdom of': 'Netherlands', }
        border['Country'] = [replacement[x] if x in replacement else x for x in border['Country']]

        df = self.treaties(df)

        test2 = pd.merge(
            pd.merge(border,
                     df,
                     on='Country'),
            df,
            left_on='country2',
            right_on='Country')

        return self.more_statistics(test2)

    def treaties(self, df):
        schengen = self.organizations['Schengen Area']
        common = self.organizations['Common Travel Area']
        unionstate = self.organizations['Union State']
        peaceandfriendship = self.organizations['Peace and Friendship']
        ca4 = self.organizations["CA4 Border Control Agreement"]
        tasman = self.organizations["Tasman Agreement"]
        andean = self.organizations["Andean Community"]
        caricom = self.organizations["Caribbean Community"]
        gcc = self.organizations["Gulf Cooperation Council"]
        eac = self.organizations["East African Community"]
        table = [schengen + common + unionstate + peaceandfriendship + ca4 + tasman + andean + caricom + gcc + eac,
                 ['Schengen'] *
                 len(schengen) + ['Common'] * len(common) + ['Union State'] * len(unionstate) +
                 ['Peace and Friendship'] * len(peaceandfriendship) + ['Central America 4'] * len(ca4) +
                 ['Trans-Tasman Agreement'] * len(tasman) + ['Andean Community'] * len(andean) +
                 len(caricom) * ['Caricom'] + len(gcc) * ['Gulf Cooperation Council'] + [
                     'East African Community'] * len(eac)]
        treaties = pd.DataFrame(table).transpose()
        treaties.columns = ['Country', 'Treaty']
        notreaty = pd.DataFrame([*set(treaties['Country']) ^ set(df['Country'])])
        notreaty['Treaty'] = None
        notreaty.columns = ['Country', 'Treaty']
        treaties = pd.concat([notreaty, treaties])
        return pd.merge(df, treaties)

    def load_islam(self):
        islam = pd.read_csv('islam.csv')
        islam.columns = ['Country', 'Total Population', 'Muslim Population',
                         'Muslim percentage of total population', 'Percentage of world Muslims (%)']
        islam = islam[['Country', 'Muslim Population',
                       'Muslim percentage of total population', 'Percentage of world Muslims (%)']]
        islam['Country'] = islam['Country'].replace(self.replacements)
        return islam

    @staticmethod
    def more_statistics(df):
        # More statistics
        df['Population difference'] = df['Population_x'] / df['Population_y']
        df['Population growth difference'] = df['Population growth_x'] / df['Population growth_y']
        df['GDP difference'] = df['GDP_x'] / df['GDP_y']
        df['GDP per capita difference'] = df['GDP per capita_x'] / df['GDP per capita_y']
        df['Population difference'] = [1 / x if x > 1 else x for x in df['Population difference']]
        df['Population growth difference'] = [1 / x if x > 1 else x for x in df['Population growth difference']]
        df['GDP difference'] = [1 / x if x > 1 else x for x in df['GDP difference']]
        df['GDP per capita difference'] = [1 / x if x > 1 else x for x in df['GDP per capita difference']]

        df['GDP sum'] = df['GDP_x'] + df['GDP_y']
        df['Population'] = df['Population_x'] + df['Population_y']
        df['GDP per capita'] = df['GDP sum'] / df['Population']
        df['cpi difference'] = df['CPI score 2021_x'] / df['CPI score 2021_y']
        df['cpi difference'] = [1 / x if x > 1 else x for x in df['cpi difference']]
        # df['db difference'] = df['DB 2019_x'] / df['DB 2019_y']
        # df['db difference'] = [1 / x if x > 1 else x for x in df['db difference']]
        df['pfi difference'] = df['pfi 2021_x'] / df['pfi 2021_y']
        df['pfi difference'] = [1 / x if x > 1 else x for x in df['pfi difference']]
        df['homicide difference'] = df['Homicide Rate_x'] / df['Homicide Rate_y']
        df['homicide difference'] = [1 / x if x > 1 else x for x in df['homicide difference']]
        df['Country Similarity'] = df[['cpi difference', 'pfi difference', 'homicide difference']].mean(axis=1)
        df['average cpi'] = df[['CPI score 2021_x', 'CPI score 2021_y']].mean(axis=1)
        df['average db'] = df[['DB 2019_x', 'DB 2019_y']].mean(axis=1)
        df['average democracy'] = df[['Overall Score_x', 'Overall Score_y']].mean(axis=1)
        df['average homicide'] = df[['Homicide Rate_x', 'Homicide Rate_y']].mean(axis=1)
        df['min democracy'] = df[['Overall Score_x', 'Overall Score_y']].min(axis=1)
        df['max democracy'] = df[['Overall Score_x', 'Overall Score_y']].max(axis=1)
        df['min GDP'] = df[['GDP_x', 'GDP_y']].min(axis=1)
        df['max GDP'] = df[['GDP_x', 'GDP_y']].max(axis=1)
        df['min population'] = df[['Population_x', 'Population_y']].min(axis=1)
        df['max population'] = df[['Population_x', 'Population_y']].max(axis=1)
        df['min cpi'] = df[['CPI score 2021_x', 'CPI score 2021_y']].min(axis=1)
        df['max cpi'] = df[['CPI score 2021_x', 'CPI score 2021_y']].max(axis=1)
        if 'Treaty_x' in df.columns:
            df['Border Status'] = np.where(df['Treaty_x'] == df['Treaty_y'], 'Open', 'Closed')
        df['Combined Population'] = df['Population_x'] + df['Population_y']
        df['viability'] = df['average cpi'] * df['average democracy'] / df['average homicide']

        return df

    def all_combos(self):
        df = self.main()

        df = self.treaties(df)

        combos = list(combinations(df['Country'], 2))
        borders = pd.DataFrame(combos).drop_duplicates()
        test2 = pd.merge(pd.merge(borders, df, left_on=0, right_on='Country'),
                         df, left_on=1, right_on='Country')

        return self.more_statistics(test2)

    def save_to_mysql(self):
        # credentials = json.loads(open('credentials.json').read())

        con = create_engine("mysql+mysqlconnector://matthew:7811mirimichi@localhost/borders")
        cpi = self.load_cpi()
        cpi.dropna(subset=['CPI score 2021'], inplace=True)
        cpi.columns = [x.replace(' ', '_') for x in cpi.columns]
        cpi.to_sql(name='CPI', con=con, if_exists='replace', index=False)

        # pfi = self.load_pfi()
        # pfi.columns = [x.replace(' ', '_') for x in pfi.columns]
        # pfi.to_sql(name='pfi', con=con, if_exists='replace')

        homicide = self.load_homicide()
        homicide.columns = [x.replace(' ', '_') for x in homicide.columns]
        homicide.to_sql(name='homicide', con=con, if_exists='replace')

        democracy = self.load_democracy()
        democracy.columns = [x.replace(' ', '_') for x in democracy.columns]
        democracy.to_sql(name='democracy', con=con, if_exists='replace')

        data = self.world_bank_data()
        data['Overall Score'] = [self.floaty(x) for x in data['Overall Score']]
        data.columns = [x.replace(' ', '_') for x in data.columns]
        #  data['Overall Score'] = [self.floaty(x) for x in data['Overall Score']]
        data.to_sql(name='world_bank', con=con, if_exists='replace')

        capitals = pd.read_csv('capitals.csv')
        capitals.columns = [x.replace(' ', '_') for x in capitals.columns]
        capitals.to_sql(name='capitals', con=con, if_exists='replace')

        presidential_trips = pd.read_csv('presidential_trips.csv')
        presidential_trips.columns = ['Region', 'Country', 'Presidential Trips', 'Trips']
        presidential_trips.columns = [x.replace(' ', '_') for x in presidential_trips.columns]
        presidential_trips.to_sql(name='presidential_trips', con=con, if_exists='replace')

        north = self.northernmost()
        north.columns = [x.replace(' ', '_') for x in north.columns]
        north.to_sql(name='north', con=con, if_exists='replace')

        # abortion = self.load_abortion()
        # abortion.columns = [x.replace(' ', '_') for x in abortion.columns]
        # abortion.to_sql(name='abortion',  con=con, if_exists='replace')

        gini = self.load_gini()
        gini.columns = [x.replace(' ', '_') for x in gini.columns]
        gini.to_sql(name='gini', con=con, if_exists='replace')

        leave = pd.read_csv("paid_leave.csv")
        leave.columns = [x.replace(' ', '_') for x in leave.columns]
        leave.to_sql(name='paid_leave', con=con, if_exists='replace')

        doing_business = pd.read_csv('edbi.csv')
        doing_business.columns = [x.replace(' ', '_') for x in doing_business.columns]
        doing_business.to_sql(name='doing_business', con=con, if_exists='replace')

        gsv = self.streetview()
        gsv['Streetview'] = gsv['Streetview'].replace(np.nan, False)
        gsv.columns = [x.replace(' ', '_') for x in gsv.columns]
        gsv.to_sql(name='google_street_view', con=con, if_exists='replace')

        area = self.load_area()
        area.columns = [x.replace(' ', '_') for x in area.columns]
        area.to_sql(name='area', con=con, if_exists='replace')


class SubNational:
    def __init__(self):
        self.australia = pd.read_excel('australia.xlsx')
        self.australia['State'] = 'Australia'
        self.germany = pd.read_excel('germany.xlsx')
        self.germany['State'] = 'Germany'
        self.brazil = pd.read_excel('brazil.xlsx')
        self.brazil['State'] = 'Brazil'
        self.china = pd.read_excel('china.xlsx')
        self.china['State'] = 'China'

    @staticmethod
    def canada():
        df = pd.read_excel('canada.xlsx')
        cols = ['Gini index on adjusted household total income', 'latest gini',
                'Gini index on adjusted household after-tax income']
        for col in cols:
            df[col] = df[col] * 100
        df['State'] = 'Canada'
        return df

    @staticmethod
    def russia():
        sheet_to_df_map = pd.read_excel('russia economy.xlsx', sheet_name=None)
        sheet_to_df_map['GDP']['Federal Subject'] = [x.strip(' ').strip('\xa0') for x in
                                                     sheet_to_df_map['GDP']['Federal Subject']]
        sheet_to_df_map['GDP per capita']['Federal Subject'] = [x.strip(' ') for x in
                                                                sheet_to_df_map['GDP per capita']['Federal Subject']]
        column_names = []
        for x in sheet_to_df_map['homicide'].columns:
            if type(x) is int:
                column_names.append(f'homicide {x}')
            else:
                column_names.append(x)
        sheet_to_df_map['homicide'].columns = column_names
        homicide = sheet_to_df_map['homicide'].copy()[['Federal Subject', 'homicide 2017']]
        homicide.columns = ['Federal Subject', 'Homicide Rate']
        life_expectancy = sheet_to_df_map['life expectancy']
        df = pd.merge(sheet_to_df_map['GDP per capita'], sheet_to_df_map['GDP'], on='Federal Subject', how='outer')
        df = pd.merge(df, homicide, on='Federal Subject', how='outer')
        df = pd.merge(df, life_expectancy, on='Federal Subject', how='outer')
        df.columns = ['Country' if x == 'Federal Subject' else x for x in df.columns]
        df['GDP'] = [x * 10 ** 9 for x in df['GDP']]
        df['State'] = 'df'
        return df

    @staticmethod
    def usa():
        usa = pd.read_excel('socioeconomicindicators.xlsx', sheet_name='Sheet1')
        usa.columns = ['Country', 'Per capita income', 'Median household income',
                       'Population 2020', 'Population growth', 'Race', '% foreign born',
                       'Life expectancy', '% of people in poverty', "Bachelor's degree",
                       '% of people without health insurance', 'Obama vote 2012',
                       'Biden vote 2020', 'Clinton vote 2016', 'Median home value',
                       'Income to home value ratio', 'Public spending per k-12 student',
                       'Public funding per k-12 student', 'cost of living index',
                       'K-12 spending as % of taxpayer income',
                       'K-12 spending as % of Per capita income',
                       'Teachers as a percent of staff salaries',
                       'Public education average post secondary', 'Literacy Rate',
                       'SAT Math score', 'Sales taxes', 'Sales tax %', 'Income tax',
                       'Income tax %', 'Property tax', 'Property tax %', 'License tax',
                       'License tax %', 'Other taxes', 'Other taxes %', 'Total taxes',
                       'Population', 'Total taxes per capita', 'Total tax rate',
                       'Income after taxes', 'PVI', 'Governor', 'Senate', 'House',
                       '% who smoke', 'Current smokers', '% Tried to quit', 'Tried to quit',
                       'Former Smokers', 'Never smoked', 'Homicide Rate', 'GDP',
                       'GDP growth rate', 'Real GDP growth rate', 'GDP per capita',
                       '% of US GDP', 'Total Area', 'latest gini', 'Gini coefficient (2015-2019)',
                       'Mean Years of Schooling']
        usa['State'] = 'United States'
        return usa

    def return_all(self, country_level=False):
        if country_level:
            init = InitializeData()
            df = init.main()
            return pd.concat([df, self.australia, self.canada(), self.germany, self.brazil,
                              self.china, self.russia(), self.usa()])
        else:
            return pd.concat([self.australia, self.canada(), self.germany, self.brazil,
                              self.china, self.russia(), self.usa()])
