all information is the most up to date I can acquire.
Sources:
* World Bank
* https://en.wikipedia.org/wiki/List_of_killings_by_law_enforcement_officers_by_country
* “Press Freedom Index.”
* TCdata360, https://tcdata360.worldbank.org/indicators/h3f86901f?country=BRA&amp;indicator=32416&amp;viz=line_chart&amp;years=2002%2C2022.
* "Global Terrorism Database": https://www.start.umd.edu/data-tools/GTD
* visa policy: https://visamap.io/
* Health care coverage: https://datacatalog.worldbank.org/search/dataset/0064780
* Diversity: https://en.wikipedia.org/wiki/List_of_countries_by_ethnic_and_cultural_diversity_level
* English: https://en.wikipedia.org/wiki/List_of_countries_by_English-speaking_population
* Corruption Perceptions Index, Transparency International
* Press Freedom Index, Reporters Without Borders
* Democracy Index, Economist Intelligence Unit
* Abortion Law: https://en.wikipedia.org/wiki/Abortion_law
* Homicide: https://en.wikipedia.org/wiki/List_of_countries_by_intentional_homicide_rate
* Google Street View: https://en.wikipedia.org/wiki/Google_Street_View_coverage#Official_coverage_by_country_%CE%BFr_territory
* Military Expenditure: https://www.sipri.org/databases/milex
* LGBT rights: https://en.wikipedia.org/wiki/LGBTQ_rights_by_country_or_territory
* Tobacco: https://en.wikipedia.org/wiki/Prevalence_of_tobacco_use
* Gun laws: https://en.wikipedia.org/wiki/Overview_of_gun_laws_by_nation
* Worldwide Governance Indicators, 2024 Update, World Bank (www.govindicators.org), Accessed on 10/30/2024.
* 